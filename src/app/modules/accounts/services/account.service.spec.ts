import { TestBed, inject } from "@angular/core/testing";
import { AccountService } from "./account.service";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

describe("AccountServiceService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [AccountService],
      imports: [HttpClientTestingModule]
    })
  );

  //Test to check the api Service
  it("expects service to fetch data", inject(
    [HttpTestingController, AccountService],
    (httpMock: HttpTestingController, service: AccountService) => {
      // We call the service
      service.getAccount().subscribe(data => {
        expect(data.length).toEqual(undefined);
      });
      // We set the expectations for the HttpClient mock
      const req = httpMock.expectOne("http://localhost:3000/account");
      expect(req.request.method).toEqual("GET");
      // Then we set the fake data to be returned by the mock
      req.flush({ data: null });
    }
  ));

  it("should be created", () => {
    const service: AccountService = TestBed.get(AccountService);
    expect(service).toBeTruthy();
  });
});
