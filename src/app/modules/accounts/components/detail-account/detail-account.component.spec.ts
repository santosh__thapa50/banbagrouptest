import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { DetailAccountComponent } from "./detail-account.component";
import { RouterTestingModule } from "@angular/router/testing";
import {
  HttpClientTestingModule
} from "@angular/common/http/testing";
import { AccountService } from "../../services/account.service";
import { GridModule } from "@progress/kendo-angular-grid";
import { HttpClientModule } from "@angular/common/http";

describe("DetailAccountComponent", () => {
  let component: DetailAccountComponent;
  let fixture: ComponentFixture<DetailAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        GridModule,
        HttpClientTestingModule,
        HttpClientModule
      ],
      declarations: [DetailAccountComponent],
      providers: [{ provide: AccountService }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAccountComponent);
    component = fixture.componentInstance;
  });

  it("accountlist should be null", async(() => {
    expect(component.accountList.length).toEqual(0);
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
