import { Component, OnInit } from "@angular/core";
import { AccountService } from "../../services/account.service";
import { AccountList, ColumnSetting } from "../../models/accounts.models";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";
import { process, State } from "@progress/kendo-data-query";
import {
  GridComponent,
  DataStateChangeEvent
} from "@progress/kendo-angular-grid";
import * as $ from "jquery";

@Component({
  selector: "app-detail-account",
  templateUrl: "./detail-account.component.html",
  styleUrls: ["./detail-account.component.scss"]
})
export class DetailAccountComponent implements OnInit {
  accountList: AccountList[] = [];
  accountDetails: AccountList = {
    id: null,
    accountNumber: 0,
    accountHolderName: null,
    accountDescription: null,
    accountHolderPhoneNumber: null
  };
  public pageSize = 10;
  public skip = 0;
  private data: Object[];
  public gridView: GridDataResult;

  constructor(private accountService: AccountService) {}

  ngOnInit() {
    this.getAccountList();
  }

  getAccountList() {
    this.accountService.getAccount().subscribe(res => {
      this.accountList = res;
    });
  }

  loadItems(): void {
    this.gridView = {
      data: this.accountList.slice(this.skip, this.skip + this.pageSize),
      total: this.accountList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadItems();
  }

  details(dataItems): void {
    this.accountDetails = dataItems;
  }
}
