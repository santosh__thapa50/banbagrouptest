import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AccountService } from "../../services/account.service";
import { CustomValidationService } from "src/app/services/custom-validation.service";
import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Component({
  selector: "app-add-account",
  templateUrl: "./add-account.component.html",
  styleUrls: ["./add-account.component.scss"]
})
export class AddAccountComponent implements OnInit {
  addAccountForm: FormGroup;
  submitted: boolean = false;

  constructor(
    public _fb: FormBuilder,
    private router: Router,
    private accountService: AccountService,
    private customValidation: CustomValidationService,
  ) {}

  ngOnInit() {
    this.addAccountForm = this._fb.group({
      accountDescription: [""],
      accountHolderName: ["", Validators.required],
      accountHolderPhoneNumber: ["",Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/)],
      accountNumber: ["", [Validators.required, this.customValidation.checkLimit(100000, 999999),RxwebValidators.unique()]]
    });
  }

  get f() {
    return this.addAccountForm.controls;
  }

  save(): void {
    this.submitted = true;
    if (this.addAccountForm.valid) {
      this.accountService
        .postAccount(this.addAccountForm.value)
        .subscribe(res => {
          this.router.navigate(["/home/account"]);
        });
    } else {
    }
  }

  public cancel(): void {
    this.addAccountForm.reset();
    this.router.navigate(["/home/account"]);
  }
}
