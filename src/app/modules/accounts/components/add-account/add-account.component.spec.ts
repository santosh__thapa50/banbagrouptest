import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { AddAccountComponent } from "./add-account.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PhoneMaskDirective } from "src/app/directive/phonenumber.directive";
import { RouterTestingModule } from "@angular/router/testing";
import { By } from "@angular/platform-browser";
import { DebugElement } from "@angular/core";
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe("AddAccountComponent", () => {
  let component: AddAccountComponent;
  let fixture: ComponentFixture<AddAccountComponent>;
  let de: DebugElement;
  let le: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [AddAccountComponent, PhoneMaskDirective]
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AddAccountComponent);
        component = fixture.componentInstance;
        component.ngOnInit();
        de = fixture.debugElement.query(By.css("form"));
        le = de.nativeElement;
      });
  }));

  it("should set submitted to true", async(() => {
    component.save();
    expect(component.submitted).toBeTruthy();
  }));

  it("forms should be invalid", async(() => {
    component.addAccountForm.controls['accountNumber'].setValue("");
    component.addAccountForm.controls['accountHolderName'].setValue("");
    expect(component.addAccountForm.valid).toBeFalsy();
  }));

  it("forms should be valid", async(() => {
    component.addAccountForm.controls['accountNumber'].setValue("123456");
    component.addAccountForm.controls['accountHolderName'].setValue(
      "Ram Shyam"
    );
    expect(component.addAccountForm.valid).toBeTruthy();
  }));

});
