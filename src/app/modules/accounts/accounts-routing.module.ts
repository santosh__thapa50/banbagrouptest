import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailAccountComponent } from './components/detail-account/detail-account.component';
import { AddAccountComponent } from './components/add-account/add-account.component';

const routes: Routes = [
  { path: "", component: DetailAccountComponent },
  { path: "add", component: AddAccountComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
