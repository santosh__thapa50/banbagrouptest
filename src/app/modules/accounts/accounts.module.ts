import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GridModule } from "@progress/kendo-angular-grid";
import { AccountsRoutingModule } from "./accounts-routing.module";
import { AddAccountComponent } from "./components/add-account/add-account.component";
import { DetailAccountComponent } from "./components/detail-account/detail-account.component";
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { PhoneMaskDirective } from "src/app/directive/phonenumber.directive";

@NgModule({
  declarations: [
    DetailAccountComponent,
    AddAccountComponent,
    PhoneMaskDirective
  ],
  imports: [
    AccountsRoutingModule,
    CommonModule,
    FormsModule,
    GridModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class AccountsModule {}
