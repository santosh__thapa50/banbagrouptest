import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailPaymentComponent } from './components/detail-payment/detail-payment.component';
import { AddPaymentComponent } from './components/add-payment/add-payment.component';

const routes: Routes = [
  { path: "", component: DetailPaymentComponent },
  { path: "add", component: AddPaymentComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
