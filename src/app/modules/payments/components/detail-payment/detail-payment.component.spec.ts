import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DetailPaymentComponent } from './detail-payment.component';
import { RouterTestingModule } from '@angular/router/testing';
import { GridModule } from "@progress/kendo-angular-grid";
import { HttpClientModule } from '@angular/common/http';


describe('DetailPaymentComponent', () => {
  let component: DetailPaymentComponent;
  let fixture: ComponentFixture<DetailPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[RouterTestingModule, GridModule, HttpClientModule],
      declarations: [ DetailPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
